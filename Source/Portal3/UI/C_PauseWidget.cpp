// Fill out your copyright notice in the Description page of Project Settings.

#include "C_PauseWidget.h"

#include "GameFramework/GameModeBase.h"
#include "Kismet/GameplayStatics.h"
#include "Portal3/Game/Portal3GameInstance.h"
#include "Components/Button.h"

void UC_PauseWidget::NativeOnInitialized()
{
    Super::NativeOnInitialized();

    if (!UnpauseButton)
    {
        return;
    }
    UnpauseButton->OnClicked.AddDynamic(this, &UC_PauseWidget::Unpause);
    MenuButton->OnClicked.AddDynamic(this, &UC_PauseWidget::Menu);
}

void UC_PauseWidget::Menu()
{
    if (!GetWorld())
    {
        return;
    }
    const auto STUGameInstatnce = GetWorld()->GetGameInstance<UPortal3GameInstance>();
    if (!STUGameInstatnce)
    {
        return;
    }
    if (STUGameInstatnce->GetMenuLevelName().IsNone())
    {
        UE_LOG(LogTemp, Error, TEXT("Menu level name is NONE"));
        return;
    }

    UGameplayStatics::OpenLevel(this, STUGameInstatnce->GetMenuLevelName());
}

void UC_PauseWidget::Unpause()
{
    if (!GetWorld() 
        || !GetWorld()->GetAuthGameMode())
    {
        return;
    }
    GetWorld()->GetAuthGameMode()->ClearPause();
}
