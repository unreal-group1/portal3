// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"

#include "C_Combinator.generated.h"

class AC_LampLever;
class AC_Lever;

UCLASS()
class PORTAL3_API AC_Combinator : public AActor
{
	GENERATED_BODY()
	
	public:	
	AC_Combinator();

	protected:
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "LampLever")
    FLinearColor GoodColor;
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "LampLever")
    FLinearColor BadColor;
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "LampLever")
    float DistanceBetweenLamp = 50.0f;
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "LampLever")
    TSubclassOf<AC_LampLever> LampLeverClass;
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "LampLever")
    TSubclassOf<AC_Lever> LeverClass;


	virtual void BeginPlay() override;
    UFUNCTION(BlueprintCallable)
    void GetAllLavers();
    UFUNCTION(BlueprintCallable)
    void GenerateLamps();
    UFUNCTION(BlueprintCallable)
    void GenerateCombination();
    UFUNCTION(BlueprintCallable)
    void ActivateLaver(AC_Lever* Lever);
    UFUNCTION(BlueprintCallable)
    void ResetLevers();

    UFUNCTION(BlueprintNativeEvent)
    void Win();
    void Win_Implementation();

	private:
    TArray<AC_Lever*> Levers;
    TArray<AC_LampLever*> LampLevers;
    int32 CountActivatedLaver = 0;
};
