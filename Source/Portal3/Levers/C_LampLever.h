// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "C_LampLever.generated.h"

class UPointLightComponent;
class AC_Lever;

UCLASS()
class PORTAL3_API AC_LampLever : public AActor
{
	GENERATED_BODY()
	
	public:	
    UPROPERTY(BlueprintReadWrite, meta = (BindComponent))
    UPointLightComponent* PointLight;

	AC_LampLever();

    UFUNCTION(BlueprintCallable)
    AC_Lever* GetLever();
    UFUNCTION(BlueprintCallable)
    void SetColor(FLinearColor Color);
    UFUNCTION(BlueprintCallable)
    void SetLever(AC_Lever* NewLever);
    UFUNCTION(BlueprintCallable)
    bool GetIsActivated();
    UFUNCTION(BlueprintCallable)
    void Pull();

	protected:
	virtual void BeginPlay() override;
	
	private:
    AC_Lever* Lever;
    bool bIsActivated;

};
