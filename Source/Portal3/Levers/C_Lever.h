// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "C_Lever.generated.h"

class AC_LampLever;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnLeverPullEvent, AC_Lever*, Lever);

UCLASS()
class PORTAL3_API AC_Lever : public AActor
{
	GENERATED_BODY()
	
	public:	
    UPROPERTY()
    int32 RandomKey;
	UPROPERTY(BlueprintAssignable, Category = "Lever|Event")
    FOnLeverPullEvent OnPull;

	AC_Lever();

    UFUNCTION(BlueprintCallable)
    AC_LampLever* GetLamp();
    UFUNCTION(BlueprintCallable)
    void SetLamp(AC_LampLever* NewLamp);
    UFUNCTION(BlueprintCallable)
    bool GetIsActivated();
    UFUNCTION(BlueprintCallable)
    virtual void Pull();
    UFUNCTION(BlueprintCallable)
    void GenerateRandomKey();

	protected:
	virtual void BeginPlay() override;

	private:	
    UPROPERTY()
    AC_LampLever* Lamp;
    UPROPERTY()
    bool bIsActivated;
};
