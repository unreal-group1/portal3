// Fill out your copyright notice in the Description page of Project Settings.

#include "C_Combinator.h"
#include "C_LampLever.h"
#include "C_Lever.h"

#include "Kismet/GameplayStatics.h"

AC_Combinator::AC_Combinator()
{
 	PrimaryActorTick.bCanEverTick = false;
}

void AC_Combinator::BeginPlay()
{
	Super::BeginPlay();

    GetAllLavers();
    GenerateLamps();
    GenerateCombination();
}

void AC_Combinator::GetAllLavers()
{
    if (!GetWorld())
    {
        return;
    }
    TArray<AActor*> FindLevers;
    AC_Lever* Lever;
    UGameplayStatics::GetAllActorsOfClass(GetWorld(), LeverClass, FindLevers);

    for (AActor* SomeActor : FindLevers)
    {
        Lever = Cast<AC_Lever>(SomeActor);
        if (Lever)
        {
            Levers.Add(Lever);
            Lever->OnPull.AddUniqueDynamic(this, &AC_Combinator::ActivateLaver);
        }
    }
}

void AC_Combinator::GenerateLamps()
{
    if (!GetWorld())
    {
        return;
    }

    FActorSpawnParameters SpawnInfo;
    SpawnInfo.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
    FTransform LampTransform;
    FVector3d LampLocation;
    int32 IndexLamp = 0;

    for (AC_Lever* Lever : Levers)
    {
        LampTransform = GetTransform();
        LampLocation = LampTransform.GetLocation();
        LampLocation.Y += DistanceBetweenLamp * IndexLamp;
        LampTransform.SetLocation(LampLocation);

        const auto LampLever = GetWorld()->SpawnActor<AC_LampLever>(LampLeverClass, LampTransform, SpawnInfo);
        LampLevers.Add(LampLever);
        LampLever->SetLever(Lever);
        LampLever->SetColor(BadColor);

        Lever->SetLamp(LampLever);

        IndexLamp++;
    }
}

void AC_Combinator::GenerateCombination()
{
    for (AC_Lever* Lever : Levers)
    {
        Lever->GenerateRandomKey();
    }

    Levers.Sort(
        [](const AC_Lever& A, const AC_Lever& B) 
    { 
        return A.RandomKey < B.RandomKey; 
    });

    int Index = 0;
    for (AC_LampLever* LampLever : LampLevers)
    {
        Levers[Index]->SetLamp(LampLever);
        Index++;
    }
}

void AC_Combinator::ActivateLaver(AC_Lever* Lever)
{
    if (CountActivatedLaver < (Levers.Num() - 1))
    {
        if (Lever->GetLamp() == LampLevers[CountActivatedLaver] 
            && Lever->GetIsActivated())
        {
            Lever->GetLamp()->SetColor(GoodColor);
            CountActivatedLaver++;
        }
        else
        {
            ResetLevers();
        }
    }
    else
    {
        CountActivatedLaver = 0;

        int32 CurrentIndex = 0;
        for (AC_Lever* CurrentLever : Levers)
        {
            if (!CurrentLever->GetLamp())
            {
                UE_LOG(LogTemp, Display, TEXT("Lamp not found"));
                return;
            }

            if (CurrentLever->GetLamp() == LampLevers[CurrentIndex] 
                && CurrentLever->GetIsActivated())
            {
                CurrentLever->GetLamp()->SetColor(GoodColor);
                CountActivatedLaver++;
            }
            else
            {
                ResetLevers();
                break;
            }
            CurrentIndex++;
        }

        Win();
    }
}

void AC_Combinator::ResetLevers()
{
    CountActivatedLaver = 0;

    for (AC_Lever* Lever : Levers)
    {
        if (Lever->GetIsActivated())
        {
            Lever->Pull();
        }
    }

    for (AC_LampLever* LampLever : LampLevers)
    {
        if (LampLever->GetIsActivated())
        {
            LampLever->Pull();
        }

         LampLever->SetColor(BadColor);
    }
}

void AC_Combinator::Win_Implementation()
{
}


