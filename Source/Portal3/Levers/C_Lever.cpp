// Fill out your copyright notice in the Description page of Project Settings.

#include "C_Lever.h"
#include "C_LampLever.h"

AC_Lever::AC_Lever()
{
	PrimaryActorTick.bCanEverTick = false;
    GenerateRandomKey();
}

void AC_Lever::BeginPlay()
{
	Super::BeginPlay();
}

void AC_Lever::GenerateRandomKey()
{
    RandomKey = rand();
}

AC_LampLever* AC_Lever::GetLamp()
{
    return Lamp;
}

void AC_Lever::SetLamp(AC_LampLever* NewLamp)
{
    Lamp = NewLamp;
}

bool AC_Lever::GetIsActivated()
{
    return bIsActivated;
}

void AC_Lever::Pull()
{
    bIsActivated = !bIsActivated;
    if (Lamp)
    {
        Lamp->Pull();
    }
    OnPull.Broadcast(this);
}

