// Fill out your copyright notice in the Description page of Project Settings.

#include "C_LampLever.h"
#include "C_Lever.h"

#include "Components/PointLightComponent.h"

AC_LampLever::AC_LampLever()
{
	PrimaryActorTick.bCanEverTick = false;
}

void AC_LampLever::BeginPlay()
{
	Super::BeginPlay();
	
}

AC_Lever* AC_LampLever::GetLever()
{
    return Lever;
}

void AC_LampLever::SetColor(FLinearColor Color)
{
    if (!PointLight)
    {
        return;
    }

    PointLight->SetLightColor(Color);
}

void AC_LampLever::SetLever(AC_Lever* NewLever)
{
    Lever = NewLever;
}

bool AC_LampLever::GetIsActivated()
{
    return bIsActivated;
}

void AC_LampLever::Pull()
{
    bIsActivated = !bIsActivated;
}

